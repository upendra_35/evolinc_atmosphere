#!/bin/bash
# Upendra Kumar Devisetty
# 11/23/15
# Script to process cuffcompare output file to generate lincRNA
# Usage: 
# sh evolinc-part-I.sh -c sample.data/cuffcompare_out_annot_no_annot.combined.gtf -g sample.data/Brassica_rapa_v1.2_genome.fa -r sample.data/Brassica_rapa_v1.2_cds.fa -b sample.data/TE_RNA_transcripts.fa 2> errors.txt > output.txt

usage() {
      echo ""
      echo "Usage : sh $0 -c cuffcompare -g genome -r CDS [-b TE_RNA] [-t CAGE_RNA] [-x Known_lincRNA]"
      echo ""

cat <<'EOF'
	-c </path/to/cuffcompare output file>

	-g </path/to/reference genome file>

	-r </path/to/cDNA reference file>

	-b </path/to/Transposable Elements file>

	-t </path/to/CAGE RNA file>
	
	-x </path/to/Known lincRNA file?

	-h Show this usage information

EOF
    exit 0
}

while getopts ":b:c:g:hr:t:x:" opt; do
  case $opt in
    b)
      blastfile=$OPTARG
      ;;
    c)
      comparefile=$OPTARG
      ;;
    h)
      usage
      exit 1
      ;;		
    g)
     referencegenome=$OPTARG
      ;;
    r)
     referenceCDS=$OPTARG
      ;;	
    t)
     cagefile=$OPTARG
      ;;
    x)
     knownlinc=$OPTARG
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done

# Creat a directory to move all the output files
mkdir output


# Extracting classcode u transcripts, making fasta file, removing transcripts > 200 and selecting protein coding transcripts
grep '"u"' $comparefile | cufflinks-2.2.1.Linux_x86_64/gffread -w \
    transcripts_u.fa -g $referencegenome - && python get_gene_length_filter.py transcripts_u.fa \
    transcripts_u_filter.fa && TransDecoder-2.0.1/TransDecoder.LongOrfs -t transcripts_u_filter.fa


# Modifying the header
sed 's/ .*//' transcripts_u_filter.fa | sed -ne 's/>//p' > transcripts_u_filter.fa.genes


# Move the transcript files to this directory transcripts_u_filter.fa.transdecoder_dir
mv transcripts_u.fa transcripts_u_filter.fa transcripts_u_filter.fa.genes transcripts_u_filter.fa.transdecoder_dir/

# Change the directory 
cd transcripts_u_filter.fa.transdecoder_dir

# Genes in the protein coding genes
sed 's/|.*//' longest_orfs.cds | sed -ne 's/>//p' | uniq > longest_orfs.cds.genes


# Remove these protein coding genes from the filter file
grep -v -f longest_orfs.cds.genes transcripts_u_filter.fa.genes > transcripts_u_filter.not.genes && \
  sed 's/^/>/' transcripts_u_filter.not.genes > temp && mv temp transcripts_u_filter.not.genes # changed name here 


# Extract fasta file
python ../extract_sequences.py transcripts_u_filter.not.genes transcripts_u_filter.fa transcripts_u_filter.not.genes.fa && \
  sed 's/ /./' transcripts_u_filter.not.genes.fa > temp && mv temp transcripts_u_filter.not.genes.fa


# Blast the fasta file to TE RNA db
for i in "$@" ; do
  if [[ $i == "$blastfile" ]] ; then
     ../ncbi-blast-2.3.0+/bin/makeblastdb -in ../$blastfile -dbtype nucl -out ../$blastfile.blast.out &&
     ../ncbi-blast-2.3.0+/bin/blastn -query transcripts_u_filter.not.genes.fa -db ../$blastfile.blast.out -out transcripts_u_filter.not.genes.fa.blast.out -outfmt 6
  else
     touch transcripts_u_filter.not.genes.fa.blast.out
  fi
done


# Filter the output to select the best transcript based on e-value and bit-score
python ../filter_sequences.py transcripts_u_filter.not.genes.fa.blast.out transcripts_u_filter.not.genes.fa.blast.out.filtered


# Modify the header in the fasta file to extract header only
grep ">" transcripts_u_filter.not.genes.fa | sed 's/>//' > transcripts_u_filter.not.genes_only


# Now remove the blast hits from the fasta file
python ../fasta_remove.py transcripts_u_filter.not.genes.fa.blast.out.filtered transcripts_u_filter.not.genes_only lincRNA.genes


# Modify the fasta header to include ">"
sed 's/^/>/' lincRNA.genes > temp && mv temp lincRNA.genes

# Extract the sequences
python ../extract_sequences-1.py lincRNA.genes transcripts_u_filter.not.genes.fa lincRNA.genes.fa

# Remove redundancy
../CAP3/cap3 lincRNA.genes.fa

# Modify the contig ace file to extract gene ids
grep -e "CO" -e "RD" lincRNA.genes.fa.cap.ace | grep -v "BS" | grep -v "AF" | uniq > lincRNA.genes.fa.cap.ace.id.txt

# Replace the fasta headers in contig file and keep just one id
python ../fasta_header_rename.py lincRNA.genes.fa.cap.ace.id.txt lincRNA.genes.fa.cap.contigs lincRNA.genes.fa.cap.contigs_renamed


# Extract gene sequences
grep ">" lincRNA.genes.fa.cap.singlets > lincRNA.genes.fa.cap.singlets.genes_only
grep ">" lincRNA.genes.fa.cap.contigs_renamed > lincRNA.genes.fa.cap.contigs_renamed.genes_only


# singlets

# Make database first
../ncbi-blast-2.3.0+/bin/makeblastdb -in ../$referenceCDS -dbtype nucl -out ../$referenceCDS.blast.out

# Blasting among themselves for chimeras for singlets 
../ncbi-blast-2.3.0+/bin/blastn -query lincRNA.genes.fa.cap.singlets -db ../$referenceCDS.blast.out -out lincRNA.genes.fa.cap.singlets_cds_blast.out -evalue 1e-30 -outfmt 6

# Filter the output
python ../linc_RNA_filter.py lincRNA.genes.fa.cap.singlets_cds_blast.out lincRNA.genes.fa.cap.singlets_cds_blast.out.filtered

# Remove these from the singlets file
grep -v -f lincRNA.genes.fa.cap.singlets_cds_blast.out.filtered lincRNA.genes.fa.cap.singlets.genes_only \
  > lincRNA.genes.fa.cap.singlets_cds_blast.out.filtered.genes

# Extract sequences from these genes
python ../extract_sequences-1.py lincRNA.genes.fa.cap.singlets_cds_blast.out.filtered.genes lincRNA.genes.fa.cap.singlets lincRNA.genes.fa.cap.singlets_cds_blast.out.filtered.genes.fa

# contigs

# Blasting among themselves for chimeras for singlets 
../ncbi-blast-2.3.0+/bin/blastn -query lincRNA.genes.fa.cap.contigs_renamed -db ../$referenceCDS.blast.out -out lincRNA.genes.fa.cap.contigs_renamed_cds_blast.out -evalue 1e-30 -outfmt 6

# Filter the output
python ../linc_RNA_filter.py lincRNA.genes.fa.cap.contigs_renamed_cds_blast.out lincRNA.genes.fa.cap.contigs_renamed_cds_blast.out.filtered


# Remove these from the singlets file
grep -v -f lincRNA.genes.fa.cap.contigs_renamed_cds_blast.out.filtered lincRNA.genes.fa.cap.contigs_renamed.genes_only \
  > lincRNA.genes.fa.cap.contigs_renamed_cds_blast.out.filtered.genes


# Extract sequences from these genes
python ../extract_sequences-1.py lincRNA.genes.fa.cap.contigs_renamed_cds_blast.out.filtered.genes lincRNA.genes.fa.cap.contigs_renamed lincRNA.genes.fa.cap.contigs_renamed_cds_blast.out.filtered.genes.fa


# Add "merged" at the end of header in contig file
sed '/>/s/$/_merged/g' lincRNA.genes.fa.cap.contigs_renamed_cds_blast.out.filtered.genes.fa > temp && mv temp lincRNA.genes.fa.cap.contigs_renamed_cds_blast.out.filtered.genes.fa

# Combine both singlets and contigs
cat lincRNA.genes.fa.cap.singlets_cds_blast.out.filtered.genes.fa lincRNA.genes.fa.cap.contigs_renamed_cds_blast.out.filtered.genes.fa > lincRNA_non_redundant_filtered.genes.fa

# Gene id's of the above
grep "^>" lincRNA_non_redundant_filtered.genes.fa > lincRNA_non_redundant_filtered.genes.only

# makeblastdb 
../ncbi-blast-2.3.0+/bin/makeblastdb -in lincRNA_non_redundant_filtered.genes.fa -dbtype nucl -out lincRNA_non_redundant_filtered_blast.out

# actual blast
../ncbi-blast-2.3.0+/bin/blastn -query lincRNA_non_redundant_filtered.genes.fa -db lincRNA_non_redundant_filtered_blast.out -out lincRNA_non_redundant_filtered_blast.out.blast.out -evalue 1e-30 -outfmt 6

# Filter the sequences
python ../linc_RNA_filter-1.py lincRNA_non_redundant_filtered_blast.out.blast.out lincRNA_non_redundant_filtered_blast.out.blast.out.uniq

# Remove these
grep -v -f lincRNA_non_redundant_filtered_blast.out.blast.out.uniq lincRNA_non_redundant_filtered.genes.only \
  > lincRNA_non_redundant_filtered.genes.only_filtered

# Extract these final lincRNA sequences
python ../extract_sequences-1.py lincRNA_non_redundant_filtered.genes.only_filtered lincRNA_non_redundant_filtered.genes.fa lincRNA_final_transcripts.fa

# Convert lincRNA fasta to bed
# Index the genome first
../bwa-0.7.12/bwa index ../$referencegenome ../$referencegenome

# Mapping the lincRNA to genome using bwa
../bwa-0.7.12/bwa bwasw -t 2 ../$referencegenome lincRNA_final_transcripts.fa > lincRNA_final_transcripts.sam

# Convert sam to bam using samtools
../samtools-bcftools-htslib-1.0_x64-linux/bin/samtools view -Shu lincRNA_final_transcripts.sam > lincRNA_final_transcripts.bam

# Convert bam to bed using bed tools
../bedtools2-2.25.0/bin/bamToBed -bed12 -i lincRNA_final_transcripts.bam > lincRNA_final_transcripts.bed

# sort the bed file
../bedtools2-2.25.0/bin/sortBed -i lincRNA_final_transcripts.bed > lincRNA_final_transcripts.sorted.bed

# promoter extraction
grep ">" lincRNA_final_transcripts.fa | cut -d "." -f 1 | sed 's/>//' > lincRNA_final_transcripts_genes_only

# Extract the coordinates from the cuffcompare file
grep -f lincRNA_final_transcripts_genes_only ../$comparefile > lincRNA_final_transcripts_genes_only.gtf

# Extracting promoter coordinates from the gtf file for the transcripts
python ../prepare_promoter_gtf.py lincRNA_final_transcripts_genes_only.gtf lincRNA_final_transcripts_genes_only.promoters.gtf

# Extracting fasta from the promoter coordinates
../cufflinks-2.2.1.Linux_x86_64/gffread lincRNA_final_transcripts_genes_only.promoters.gtf -w lincRNA_final_transcripts.promoters.fa -g ../$referencegenome

# Demographics
../quast-3.0/quast.py lincRNA_final_transcripts.fa -o lincRNA_final_transcripts_demographics

sed 's/contig/lincRNA/g' lincRNA_final_transcripts_demographics/report.txt > lincRNA_final_transcripts_demographics.txt
grep -v -e "^Total" -e "^N" -e "^L" lincRNA_final_transcripts_demographics.txt > temp && mv temp lincRNA_final_transcripts_demographics.txt
sed -e 's/#//' -e 's/ //' lincRNA_final_transcripts_demographics.txt > temp && mv temp lincRNA_final_transcripts_demographics.txt

python ../lincRNA_count.py *fa > lincRNA_final_transcripts_counts.txt

# Update the gf file 
python ../update_gtf.py lincRNA_final_transcripts.fa ../$comparefile lincRNA_final_transcripts_updated.gtf

# Move all the files to a direcotry named output
cp lincRNA_final_transcripts.fa lincRNA_final_transcripts.bed lincRNA_final_transcripts.promoters.fa lincRNA_final_transcripts_counts.txt lincRNA_final_transcripts_demographics.txt lincRNA_final_transcripts_updated.gtf ../output

# optional - 1 CAGE data
for i in "$@" ; do
  if [[ $i == "$cagefile" ]] ; then
     python ../gff2bed.py ../$cagefile AnnotatedPEATPeaks.bed &&
     ../bedtools2-2.25.0/bin/sortBed -i AnnotatedPEATPeaks.bed > AnnotatedPEATPeaks.sorted.bed &&
     ../bedtools2-2.25.0/bin/closestBed -a lincRNA_final_transcripts.sorted.bed -b AnnotatedPEATPeaks.sorted.bed -s -D a > closest_output.txt &&
     python ../closet_bed_compare.py closest_output.txt lincRNA_final_transcripts.fa lincRNA_CAGE_final_transcripts.fa &&
     mv lincRNA_CAGE_final_transcripts.fa ../output
 fi
done

# optional - 2 Known lincRNA
for i in "$@" ; do
  if [[ $i == "$knownlinc" ]] ; then
     python ../gff2bed.py ../$knownlinc Atha_known_lncRNAs.bed &&
     ../bedtools2-2.25.0/bin/sortBed -i Atha_known_lncRNAs.bed > Atha_known_lncRNAs.sorted.bed &&
     ../bedtools2-2.25.0/bin/intersectBed -a lincRNA_final_transcripts.sorted.bed -b Atha_known_lncRNAs.sorted.bed > intersect_output.txt &&
     python ../interesect_bed_compare.py intersect_output.txt lincRNA_final_transcripts.fa lincRNA_overlapping_known_final_transcripts.fa &&
     mv lincRNA_overlapping_known_final_transcripts.fa ../output
  fi
done

# remove all the other files
rm -r ../transcripts_u_filter.fa.transdecoder_dir

echo "All necessary files written to output"
echo "Finished Evolinc-part-I!"
